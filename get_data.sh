#!/usr/bin/env bash


pushd data

wget https://api.bitbucket.org/2.0/repositories/jpblaschke/cufinufft-error/downloads/output-gpu.npy
wget https://api.bitbucket.org/2.0/repositories/jpblaschke/cufinufft-error/downloads/output-cpu.npy
wget https://api.bitbucket.org/2.0/repositories/jpblaschke/cufinufft-error/downloads/call-monitor-gpu.tar
wget https://api.bitbucket.org/2.0/repositories/jpblaschke/cufinufft-error/downloads/call-monitor-cpu.tar

tar -xvf call-monitor-cpu.tar
tar -xvf call-monitor-gpu.tar

popd
