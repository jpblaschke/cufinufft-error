#!/usr/bin/env python
# -*- coding: utf-8 -*-



import pycuda.autoinit # NOQA:401
from   pycuda.gpuarray import GPUArray, to_gpu

from cufinufft   import cufinufft
from callmonitor import load

import numpy  as np
import PyNVTX as nvtx

from sys     import argv
from os.path import join



@nvtx.annotate("forward_gpu")
def forward_gpu(ugrid, H_, K_, L_, support, M, N, recip_extent, use_recip_sym):
    """Apply the forward, NUFFT2- problem -- CUDA Implementation."""

    # Set up data types/dim/shape of transform
    complex_dtype = np.complex128
    dtype         = np.float64
    dim           = 3
    shape         = (M, M, M)
    tol           = 1e-12

    # Ensure that H_, K_, and L_ have the same shape
    assert H_.shape == K_.shape == L_.shape

    # Apply Support
    ugrid *= support  # /!\ overwrite

    # Copy input data to Device (if not already there)
    nvtx.RangePushA("forward_gpu:to_gpu") #------------------------------------A
    if not isinstance(H_, GPUArray):
        # Due to a change to the cufinufft API, these need to be re-ordered
        # H_gpu = to_gpu(H_)
        # K_gpu = to_gpu(K_)
        # L_gpu = to_gpu(L_)
        H_gpu = to_gpu(L_)
        K_gpu = to_gpu(K_)
        L_gpu = to_gpu(H_)
        ugrid_gpu = to_gpu(ugrid.astype(complex_dtype))
    else:
        # Due to a change to the cufinufft API, these need to be re-ordered
        # H_gpu = H_
        # K_gpu = K_
        # L_gpu = L_
        H_gpu = L_
        K_gpu = K_
        L_gpu = H_
        ugrid_gpu = ugrid.astype(complex_dtype)
    nvtx.RangePop() # ---------------------------------------------------------P

    # Check if recip symmetry is met
    if use_recip_sym:
        assert np.all(np.isreal(ugrid))

    # Allocate space on Device
    # nuvect = np.zeros(N, dtype=np.complex)
    nvtx.RangePushA("forward_gpu:GPUArray") # ---------------------------------A
    nuvect_gpu = GPUArray(shape=(N,), dtype=complex_dtype)
    nvtx.RangePop() # ---------------------------------------------------------P

    #___________________________________________________________________________
    # Solve the NUFFT
    #
    nvtx.RangePushA("forward_gpu:setup_cufinufft") # --------------------------A

    # Set NUPts
    plan = cufinufft(
        2, shape, -1, tol, dtype=dtype, gpu_method=1, gpu_device_id=0
    )
    plan.set_pts(H_gpu, K_gpu, L_gpu)

    nvtx.RangePop() # ---------------------------------------------------------P

    # Solve NUFFT
    nvtx.RangePushA("forward_gpu:nuFFT") # ------------------------------------A
    plan.execute(nuvect_gpu, ugrid_gpu)
    nvtx.RangePop() # ---------------------------------------------------------P

    #
    #---------------------------------------------------------------------------

    # Copy result back to host -- if the incoming data was on host
    nvtx.RangePushA("forward_gpu:get") # --------------------------------------A
    if not isinstance(H_, GPUArray):
        nuvect = nuvect_gpu.get()
    else:
        nuvect = nuvect_gpu
    nvtx.RangePop() #----------------------------------------------------------P

    return nuvect / M**3



def run(dst="data"):

    #___________________________________________________________________________
    # LOAD profiling data
    #

    data         = join("data", "call-monitor-gpu")
    forward_name = "forward_gpu"

    db           = load(data)
    args, kwargs = db.get(forward_name, 1)

    #
    #---------------------------------------------------------------------------


    #___________________________________________________________________________
    # RUN forward operation
    #

    nuvect = forward_gpu(*args, **kwargs)

    #
    #---------------------------------------------------------------------------


    #___________________________________________________________________________
    # CHECK the result
    #

    nuvect_cpu   = np.load(join("data", "output-cpu.npy"))
    diff_gpu_cpu = np.sum(nuvect_cpu - nuvect)

    # np.save(join("data", "new.npy"), nuvect);
    np.save(join(dst, "new.npy"), nuvect);

    print(f"nuvect_cpu - nuvect = {diff_gpu_cpu}")
    #
    #---------------------------------------------------------------------------



if __name__ == "__main__":
    if len(argv) > 1 :
        run(dst=argv[1])
    else:
        run()
