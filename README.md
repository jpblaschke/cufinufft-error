This tests 2 versions of cufinufft
1. `cufinufft-old.py` tests the version in `cufinufft/cufinufft-old/` which is
   a recent but not the latest version.
2. `cufinufft-new.py` tests the version in `cufinufft/cufinufft-new/` which is
   the latest version.

Both tests compare to the results of a fairly old non-cuda finufft -- you can
get the results and inputs files using the `get_data.sh` command. Note: this
downloads approx. 3GB.

## Installation

1. Get submodules `git submodule update --init`
2. Install dependencies

    ```
    pip install PyNVTX callmonitor
    ```

## Notes

* You might need to run with `mpirun -n 1` (depending on your MPI setup -- this
  should not be necessary, but on Summit you must run with MPI enabled.)
